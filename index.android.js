import React, { Component, AppRegistry } from 'react-native';
import Root           from '../ReactNativeWebGroceryApp/app/native/containers/Root';
import configureStore from '../ReactNativeWebGroceryApp/app/store/configureStore.prod.js';

const store = configureStore();

class ReactNativeelloWorld extends Component {
  render() {
    return (
      <Root store={store} />
    );
  }
}

AppRegistry.registerComponent('ReactNativeWebGroceryApp', () => ReactNativeelloWorld);
