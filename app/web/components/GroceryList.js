import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

// actions - items
import {
    removeItemStart,
} from '../../reducers/items/items-reducer';

const ListItem = require('./ListItem');

export class GroceryList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const itemList = this.props.itemList;

        return (
            <div className="View">
                <div className="ListView">
                    {
                        itemList.map(function(item, index) {
                            const handleListItemClick = this.handleListItemClick.bind(this, item);
                            return <ListItem className="ListItem" key={index} item={item} onClick={handleListItemClick} />
                        }, this)
                    }
                </div>
            </div>
        )
    }

    handleListItemClick(item, event) {
        const dispatch = this.props.dispatch;
        const title = `${item.title} complete?`;

        // confirm
        if(window.confirm(title)) {
            // remove item
            dispatch(removeItemStart(item.key));
        }
    }
}

// connect states
GroceryList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const select = state => state;

// wrap the component to inject dispatch and state into it
export default connect(select)(GroceryList);