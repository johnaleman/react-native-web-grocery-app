import React, { Component, PropTypes} from 'react';

export default class MenuItem extends Component {
    render() {
        const {handleMenuItemClick} = this.props;
        return (
            <div className="menu-item" onClick={handleMenuItemClick}>Menu Item</div>
        );
    }
}

MenuItem.propTypes = {
    handleMenuItemClick: PropTypes.func.isRequired
};