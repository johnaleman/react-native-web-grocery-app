import React, {Component} from 'react';

class ListItem extends Component {
    render() {
        return (
            <div className="touchable-highlight" onClick={this.props.onClick}>
                <div className="view li">
                    <div className="text li-text title">{this.props.item.title}</div>
                </div>
            </div>
        );
    }
}

module.exports = ListItem;