'use strict';

import React, {Component} from 'react';

class ActionButton extends Component {
    render() {
        return (
            <div className="View">
                <button className="ActionButton" onClick={this.props.onClick}>{this.props.title}</button>
            </div>
        );
    }
}

module.exports = ActionButton;