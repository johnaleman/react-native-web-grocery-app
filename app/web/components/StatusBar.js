'use strict';
import React, {Component} from 'react';

class StatusBar extends Component {
    render() {
        return (
            <div className="view">
                <div className="view statusbar" />
                <div className="view navbar">
                    <div className="text navbarTitle">{this.props.title}</div>
                </div>
            </div>
        );
    }
}

module.exports = StatusBar;