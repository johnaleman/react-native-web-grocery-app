// core
import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

// dumb components
const ActionButton = require('../components/ActionButton');
import GroceryList from '../components/GroceryList';
import StatusBar from '../components/StatusBar';

// actions - items
import {
    onAppInit,
    loadItems,
    addItemStart
} from '../../reducers/items/items-reducer';

// app entry
class App extends Component {
    constructor(props) {
        super(props);

        // events
        this.onClick = this.onClick.bind(this);
    }

    componentWillMount() {
        const {dispatch} = this.props;

        // init
        dispatch(onAppInit());
        dispatch(loadItems());
    }

    render() {
        console.log('[APP] VERSION: 1.0.1');

        const {dispatch, items} = this.props;
        const itemList = items.data.itemList;

        return (
            <div className="view react-native-web">
                <StatusBar title="Grocery List"/>
                <ActionButton onClick={this.onClick} title="Add New Item" />
                <GroceryList itemList={itemList} />
            </div>
        );
    }

    onClick() {
        const {dispatch} = this.props;
        const title = 'Add New Item';
        const defaultVal = 'water';

        // alert
        let itemName = window.prompt(title, defaultVal);
        if(itemName != null && itemName !== '') {
            dispatch(addItemStart(itemName));
        }
    }
}

// required props
App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    items: PropTypes.object.isRequired,
};

const select = state => state;

// wrap the component to inject dispatch and state into it
export default connect(select)(App);
