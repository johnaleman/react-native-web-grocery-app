// libs
import request from 'superagent';
import update from 'react-addons-update';
import assign from 'object-assign';

// constants
const POST_FETCH_START = 'POST_FETCH_START';
const POST_FETCH_COMPLETE = 'POST_FETCH_COMPLETE';

const baseURL = 'http://jsonplaceholder.typicode.com';

// actions creators
export function fetchPost() {
    return dispatch => {
        dispatch({ type: POST_FETCH_START });

        let $promise = new Promise((resolve, reject) => {
            let api = '/posts/1';

            request.get(baseURL + api)
            .query({})
            .end((err, res) => {
                    if(err) {
                        reject(err);
                    } else {
                        resolve(res.body);
                    }
                }
            );
        });

        $promise.then(data => {
            dispatch({ type: POST_FETCH_COMPLETE, data: data });
        });
    };
}

// init state
const initialState = {
    name: 'postReducer',
    data: {
        loading: false,
        itemList: []
    }
};

// reducer
export function postReducer(state = initialState, action) {
    const item = action.data;
    const initialArray = state.data.itemList;
    let newArray = [];

    switch(action.type) {
        case POST_FETCH_START:
            // code
            break;

        case POST_FETCH_COMPLETE:
            newArray = [item];
            return assign({}, state, {
                data: assign({}, state.data, {
                    itemList: newArray
                })
            });

            break;

        default:
            break;
    }

    return state
}