/*
 * Data Model
 */

import { Record } from 'immutable';

export const Item = new Record({
    key: null,
    title: null
});
