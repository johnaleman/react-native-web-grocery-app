/*
 * Items Module
 */

// libs
import update from 'react-addons-update';
import assign from 'object-assign';

// firebase
import { FirebaseList } from '../../firebase/firebase-list';
import { Item } from './item-model';

// constants
export const ITEM_FETCH_START = 'ITEM_FETCH_START';
export const LOAD_ITEMS_SUCCESS = 'LOAD_ITEMS_SUCCESS';

export const ADD_ITEM_START = 'ADD_ITEM_START';
export const ADD_ITEM_SUCCESS = 'ADD_ITEM_SUCCESS';

export const REMOVE_ITEM_START = 'REMOVE_ITEM_START';
export const REMOVE_ITEM_SUCCESS = 'REMOVE_ITEM_SUCCESS';

/*
 * map actions to firebase api
 *
 * onAdd
 * onLoad
 * onRemove
 */
const itemReq = new FirebaseList({
    onAdd: addItemSuccess,
    onLoad: loadItemSuccess,
    onRemove: removeItemSuccess
}, Item);

// action creators
export function onAppInit() {
    console.log('onAppInit');

    return {
        type: ITEM_FETCH_START,
        payload: {},
        meta: {}
    }
}

// item list
export function loadItems() {
    return dispatch => {
        itemReq.path = 'items';
        itemReq.subscribe(dispatch); // fetch data from firebase
    }
}

export function loadItemSuccess(itemList) {
    return  dispatch => {
        dispatch({ type: LOAD_ITEMS_SUCCESS, data: itemList });
    };
}

// create item
export function addItemStart(value) {
    return dispatch => {
        console.log('addItemStart');

        dispatch({ type: ADD_ITEM_START, data: {} });

        const params = {
            title: value
        };

        itemReq.path = 'items';
        itemReq.push(params);
    }
}

export function addItemSuccess(item) {
    return dispatch => {
        dispatch({ type: ADD_ITEM_SUCCESS, data: item });
    }
}

// remote item
export function removeItemStart(value) {
    return dispatch => {
        dispatch({ type: REMOVE_ITEM_START, data: {} });

        itemReq.path = 'items';
        itemReq.remove(value);
    }
}

export function removeItemSuccess(item) {
    return dispatch => {
        dispatch({ type: REMOVE_ITEM_SUCCESS, data: item });
    }
}

// init state
const initialState = {
    name: 'itemsReducer',
    data: {
        loading: false,
        itemList: []
    }
};

// reducer
export function itemsReducer(state = initialState, action) {
    const item = action.data;
    const initialArray = state.data.itemList;
    let newArray = [];

    switch(action.type) {
        case ITEM_FETCH_START:
            // code
            break;

        case LOAD_ITEMS_SUCCESS:
            return assign({}, state, {
                data: assign({}, state.data, {
                    itemList: action.data
                })
            });
            break;

        case ADD_ITEM_START:
            // code
            break;

        case ADD_ITEM_SUCCESS:
            newArray = update(initialArray, {$push: [item]});

            return assign({}, state, {
                data: assign({}, state.data, {
                    itemList: newArray
                })
            });
            break;

        case REMOVE_ITEM_START:
            // code
            break;

        case REMOVE_ITEM_SUCCESS:
            // find index
            const index = initialArray.map(function(o) {
                return o.key;
            }).indexOf(item.key);

            newArray = update(initialArray, {$splice: [[index, 1]]});

            return assign({}, state, {
                data: assign({}, state.data, {
                    itemList: newArray
                })
            });
            break;
    }

    return state;
}