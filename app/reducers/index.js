import { combineReducers } from 'redux';

// reducer
import {colorsReducer} from '../components/colors/reducer';
import {itemsReducer} from './items/items-reducer';

export default combineReducers({
    colors: colorsReducer,
    items: itemsReducer
});