import request from 'superagent';

const baseURL = 'http://jsonplaceholder.typicode.com';

/**
 * Hit the endpoint with the proper query.
 * Return a promise that resolves with the response.
 */
export function exampleRequest() {
    return new Promise((resolve, reject) => {
        let api = '/posts/1';

        request.get(baseURL + api)
            .query({})
            .end((err, res) => {
                    if(err) {
                        reject(err);
                    } else {
                        resolve(res.body);
                    }
                }
            );
    });
}
