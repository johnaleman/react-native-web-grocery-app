import {
    EVENT_CLICK,
    EVENT_MENU_ITEM_CLICK
} from './Constants';

export function handleEventClick() {
    return {type: EVENT_CLICK};
}

export function handleMenuItemClick() {
    return {type: EVENT_MENU_ITEM_CLICK}
}