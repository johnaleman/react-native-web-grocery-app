// core
import React, {Component, PropTypes} from 'react';
import {connect}  from 'react-redux';
import {AlertIOS, View} from 'react-native';

// style
import {styles} from '../styles/styles';

// dumb components
import ActionButton from '../components/ActionButton';
import GroceryList from '../components/GroceryList';
import StatusBar from '../components/StatusBar';

// actions - items
import {
    onAppInit,
    loadItems,
    addItemStart
} from '../../reducers/items/items-reducer';

// app entry
class App extends Component {
    constructor(props) {
        super(props);

        // events
        this.onClick = this.onClick.bind(this);
    }

    componentWillMount() {
        const {dispatch} = this.props;

        // init
        dispatch(onAppInit());
        dispatch(loadItems());
    }

    render() {
        const {dispatch, items} = this.props;
        const itemList = items.data.itemList;

        return (
            <View style={styles.reactNativeWeb}>
                <StatusBar title="Grocery List"/>
                <ActionButton onPress={this.onClick} title="Add" />
                <GroceryList itemList={itemList}/>
            </View>
        )
    }

    onClick() {
        const {dispatch} = this.props;
        const title = 'Add New Item';
        const defaultVal = 'water';

        // alert
        AlertIOS.prompt(
            title,
            null,
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel'
                },
                {
                    text: 'Add',
                    onPress: (itemName) => {
                        if(itemName != null && itemName !== '') {
                            dispatch(addItemStart(itemName));
                        }
                    }
                },
            ],
            'plain-text'
        );
    }
}

// required props
App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    items: PropTypes.object.isRequired,
};

const select = state => state;

// wrap the component to inject dispatch and state into it
export default connect(select)(App);
