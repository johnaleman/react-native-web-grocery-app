import React, {Component, PropTypes} from 'react';
import {Provider} from 'react-redux';

// app
import App from './App';

export default class Root extends Component {
    render() {
        return (
            <Provider store={this.props.store}>
                <App />
            </Provider>
        );
    }
}

// required prop types
Root.propTypes = {
    store: PropTypes.object.isRequired,
};
