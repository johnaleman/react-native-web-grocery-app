import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import ReactNative from 'react-native';

// actions - items
import {
    removeItemStart,
} from '../../reducers/items/items-reducer';

import ListItem from './ListItem';

const styles = require('../styles/styles');

// react native components
const {
    ListView,
    View,
    AlertIOS,
} = ReactNative;

export class GroceryList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const itemList = this.props.itemList;
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            dataSource: ds.cloneWithRows(itemList),
        };

        return (
            <View>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.map.bind(this)} // list item
                    enableEmptySections={true}
                    style={styles.listview}/>
            </View>
        )
    }

    handleListItemClick(item, event) {
        const dispatch = this.props.dispatch;
        const title = 'Complete?';

        // confirm
        AlertIOS.alert(
            title,
            null,
            [
                {
                    text: 'Complete',
                    onPress: (text) => {
                        // remove item
                        dispatch(removeItemStart(item.key));
                    },
                },
                {
                    text: 'Cancel',
                    onPress: () => {}
                }
            ]
        );
    }

    map(item) {
        const handleListItemClick = this.handleListItemClick.bind(this, item);
        return <ListItem item={item} onPress={handleListItemClick} />;
    }
}

// connect states
GroceryList.propTypes = {
    dispatch: PropTypes.func.isRequired
};

const select = state => state;

// wrap the component to inject dispatch and state into it
export default connect(select)(GroceryList);